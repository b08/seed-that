# @b08/seed-that
Seeds new npm package from seed.

# limitations
1. Only gitlab.com is supported
2. ssh protocol is used for cloning
2. Only account-marked libraries are supported, i.e. 

# install
npm i -g @b08/seed-that@latest

# usage 
```
seed-that [[<git_user>/]<npm_user>/]<newLib> [[<git_user>/]<user>/<seed>] [--type <type>]
```
Parameters:
1. Name of new package. Mandatory. If git(gitlab) user is not specified, then it is the same as npm user. If neither of users are specified, user from options file is used. \
2. Name of the seed. Optional. If not specified, then options are used.\
3. Type of the library. Also can be taken from options. Only used if seed supports publishing metrics.

# configuration
In the folder where you will create your packages, create seed-that.json file, with following structure:
```
{
  "type": "support",
  "gitlabToken": "your token",
  "defaults": {
    "npmUser": "b08",
    "gitUser": "b08",
    "seed": "@b08/library-seed"
  },
  "env": {    
    "variable": "value"
  }
}
```
First option is the library type, used types are: support, dry, feature or generator\
GitlabToken is used to create repository on gitlab and set its parameters\
Default are used if command-lines parameters are not specified\
Environment variables are all pushed to gitlab repository.\
There is one special environment variable - gitKey, if it is not specified in the options, it will be read from your id_rsa file.

# prerequisite
Git should be ready to work with gitlab.com, i.e. in your home directory, you should have id_rsa file, and known_hosts file should contain gitlab.com signature. \
To verify, git clone any repository from gitlab.com.

# what seed-that does
1. creates a repository on gitlab
2. clones seed repository to local folder
3. sets 'origin' remote to your new repository
4. sets 'seed' remote to the seed
5. pushes master branch

6. Sets merge strategy settings in created project: fast-forward and  only allow if pipeline passed
7. Sets environment variables from prerequisite files
8. Protects master branch

9. creates 'feature/first-implementation'  branch for first version of your library
10. in that branch updates library name, cleans readme and then commits and pushes
11. executes 'npm i' in the new repository for convenience

# library name update
There might be several files in the seed, containing seed name, for instance gitlab-ci.yml \
It will be replaced with the name of new library. For example b08/library-seed will be replaced with b08/object.\
Also there is a line of code used for publishing, library type will be set to new value if type is specified either in cli parameter or options.

# clone-that
When project is already seeded, to clone it on another machine, clone-that command can be used.
```
clone-that <npm_user>/<newLib>
```
it clones the repository to local folder, reads "seeded from %seed%" from readme.md, configures seed-remote according to it, and installs node modules.