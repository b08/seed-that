import * as request from "request-promise-native";
import { Options } from "../options/options.type";

const gitlabUrl = "https://gitlab.com/api/v4";

function fullUrl(url: string): string {
  return gitlabUrl + url;
}

async function run<T = any>(options: Options, method: string, url: string, body: any): Promise<T> {
  const opts = {
    method,
    headers: {
      "Private-Token": options.gitlabToken
    },
    url: fullUrl(url),
    json: true,
    proxy: options.proxy,
    body
  };

  const result: T = await request(opts);
  return result;
}

function post<T = any>(options: Options, url: string, body: any): Promise<T> {
  return run(options, "POST", url, body);
}

async function del<T = any>(options: Options, url: string, body: any = {}): Promise<T> {
  return run(options, "DELETE", url, body);
}

export const gitlabRequest = {
  post,
  del
};
