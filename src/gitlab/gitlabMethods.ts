
import { Options } from "../options/options.type";
import { Project } from "../project/project.type";
import { gitlabRequest } from "./gitlabRequest";

export async function createRepoOnGitLab(project: Project, options: Options): Promise<string> {
  console.log(`creating repo "${project.name}" on gitlab`);
  const body = {
    name: project.name,
    issues_enabled: true,
    merge_requests_enabled: true,
    jobs_enabled: true,
    visibility: "public",
    only_allow_merge_if_pipeline_succeeds: true,
    only_allow_merge_if_all_discussions_are_resolved: true,
    merge_method: "ff"
  };
  const result = await gitlabRequest.post(options, "/projects", body);
  console.log(`project ${result.id} created`);
  return result.id;
}

export async function setRepoEnvVariables(projectId: string, options: Options): Promise<void> {
  console.log("Setting environment variables");
  const promises = Object.keys(options.env).map(k => setVar(k, options.env[k], projectId, options));
  await Promise.all(promises);
  console.log("Finished with environment variables");
}

async function setVar(key: string, value: string, projectId: string, options: Options): Promise<void> {
  const body = {
    id: projectId,
    key,
    value,
    protected: true
  };
  await gitlabRequest.post(options, `/projects/${projectId}/variables`, body);
}

export async function setMasterProtection(projectId: string, options: Options): Promise<void> {
  await gitlabRequest.del(options, `/projects/${projectId}/protected_branches/master`);
  const body = {
    id: projectId,
    name: "master",
    merge_access_level: 40,
    push_access_level: 0
  };
  await gitlabRequest.post(options, `/projects/${projectId}/protected_branches`, body);
}
