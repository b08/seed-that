import { installNodeModules } from "./additionalSteps/installModules";
import { changeSeedRemote, cloneRepository, checkoutFirstBranch } from "./git/gitSteps";
import { getTargetProject } from "./project/getProjectFromArgs";
import { getSourceFromReadme } from "./additionalSteps/getSourceFromReadme";
import { readOptions } from "./options/readOptions";

async function cloneThat(): Promise<void> {
  const options = await readOptions();
  const target = getTargetProject(options);
  await cloneRepository(target, target);
  await checkoutFirstBranch(target);
  const source = await getSourceFromReadme(target, options);
  await changeSeedRemote(source, target);
  await installNodeModules(target);
}

cloneThat()
  .catch(err => console.error(err));
