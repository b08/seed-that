import { spawnIt } from "@b08/spawn";
import { Project } from "../project/project.type";
import { targetPath } from "./targetPath";

function gitPath(project: Project): string {
  return `git@gitlab.com:${project.gitUser}/${project.name}.git`;
}

export async function cloneRepository(source: Project, target: Project): Promise<void> {
  const sourcePath = gitPath(source);
  console.log(`Cloning ${sourcePath} to ${target.name} `);
  await spawnIt("git", ["clone", sourcePath, target.name]);
  console.log("Finished clone");
}


export async function changeTargetRemote(target: Project): Promise<void> {
  const path = targetPath(target);
  console.log(`Setting source remote`);
  await spawnIt("git", ["remote", "set-url", "origin", gitPath(target)], { cwd: path });
  console.log("Finished setting remote");
}

export function wait(milliseconds: number): Promise<void> {
  return new Promise(resolve => setTimeout(resolve, milliseconds));
}

export async function changeSeedRemote(source: Project, target: Project): Promise<void> {
  console.log(`Setting seed remote`);
  await spawnIt("git", ["remote", "add", "seed", gitPath(source)], { cwd: targetPath(target) });
  await wait(500);
  await spawnIt("git", ["fetch", "seed"], { cwd: targetPath(target) });
  console.log("Finished setting remote");
}

export async function pushMaster(target: Project): Promise<void> {
  const path = targetPath(target);
  console.log(`Pushing master`);
  await spawnIt("git", ["push"], { cwd: path });
  console.log("Finished pushing master");
}

const firstBranch = "feature/first-implementation";
export async function createFirstBranch(target: Project): Promise<void> {
  console.log(`Creating branch`);
  await spawnIt("git", ["branch", firstBranch], { cwd: targetPath(target) });
  await checkoutFirstBranch(target);
  console.log("Finished creating branch");
}

export async function checkoutFirstBranch(target: Project): Promise<void> {
  try {
    await spawnIt("git", ["checkout", firstBranch], { cwd: targetPath(target) });
  } catch (ex) {
    // do nothing, this means first branch was already merged and deleted
  }
}

export async function pushChangesToBranch(target: Project): Promise<void> {
  const path = targetPath(target);
  console.log(`Pushing changes`);
  await spawnIt("git", ["commit", "-a", "-m", `"first commit"`], { cwd: path });
  await spawnIt("git", ["push", "--set-upstream", "origin", firstBranch], { cwd: path });
  console.log("Finished pushing changes");
}


