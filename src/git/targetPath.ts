import { Project } from "../project/project.type";
export function targetPath(target: Project): string {
  const cwd = process.cwd();
  return cwd + "/" + target.name;
}
