import { installNodeModules } from "./additionalSteps/installModules";
import { replaceProjectInfoInFiles } from "./additionalSteps/replaceProjectInfoInFiles";
import { replaceReadme } from "./additionalSteps/replaceReadme";
import { changeSeedRemote, changeTargetRemote, cloneRepository, createFirstBranch, pushChangesToBranch, pushMaster } from "./git/gitSteps";
import { createRepoOnGitLab, setMasterProtection, setRepoEnvVariables } from "./gitlab/gitlabMethods";
import { readOptions } from "./options/readOptions";
import { getSourceProject, getTargetProject } from "./project/getProjectFromArgs";

async function seedThat(): Promise<void> {
  const options = await readOptions();
  const source = getSourceProject(options);
  const target = getTargetProject(options);
  const projectId = await createRepoOnGitLab(target, options);
  await setRepoEnvVariables(projectId, options);
  await cloneRepository(source, target);
  await changeTargetRemote(target);
  await changeSeedRemote(source, target);
  await pushMaster(target);
  await setMasterProtection(projectId, options);
  await createFirstBranch(target);
  await replaceProjectInfoInFiles(source, target, options);
  await replaceReadme(source, target, options);
  await pushChangesToBranch(target);
  await installNodeModules(target);
}

seedThat()
  .catch(err => console.error(err));
