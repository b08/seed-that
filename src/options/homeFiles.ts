import * as fse from "fs-extra";
import * as os from "os";

function readFile(name: string, message: string): Promise<string> {
  try {
    const sshDir = os.homedir() + "/.ssh/";
    return fse.readFile(sshDir + name, "utf8");
  } catch (ex) {
    console.error(ex);
    throw new Error(`Error reading ${message}, file name: ${name}`);
  }
}

export const getGitPrivateKey = () => readFile("id_rsa", "git private key");
