import * as fse from "fs-extra";
import { getGitPrivateKey } from "./homeFiles";
import { Defaults, Options } from "./options.type";
import { getTypeFromParameters } from "./getTypeFromParameters";

const optionsFile = "./seed-that.json";

const defaults: Defaults = {
  npmUser: "b08",
  gitUser: "b08",
  seed: "b08/library-seed"
};

export async function readOptions(): Promise<Options> {
  const options = await readAndParseOptions();
  const gitKey = options.env && options.env.gitKey;
  return {
    ...options,
    defaults: { ...defaults, ...options.defaults },
    type: getTypeFromParameters() || options.type,
    env: {
      ...options.env,
      gitKey: gitKey || await getGitPrivateKey(),
    }
  };
}

async function readAndParseOptions(): Promise<Options> {
  try {
    const file = await fse.readFile(optionsFile, "utf8");
    const options = JSON.parse(file);
    return options;
  } catch (ex) {
    console.error(`Create file ${optionsFile} at current location, package description has information on its structure`);
  }
}
