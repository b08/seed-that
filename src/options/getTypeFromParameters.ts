import { argv } from "yargs";

export function getTypeFromParameters(): string {
  return argv.type;
}
