export interface Options {
  gitlabToken: string;
  defaults: Defaults;
  env: Environment;
  type: string;
  proxy: string;
}

export interface Environment {
  statisticsUrl: string;
  statisticsPassword: string;
  npmToken: string;
  gitKey: string;
}

export interface Defaults {
  npmUser: string;
  gitUser: string;
  seed: string;
}
