export interface Project {
  name: string;
  npmUser: string;
  gitUser: string;
}
