import { Project } from "./project.type";
import { Options } from "../options/options.type";

function isSeedThat(arg: string): boolean {
  if (arg === "seed-that" || arg === "clone-that") { return true; }
  if (arg.endsWith("seed-that.js") || arg.endsWith("clone-that.js")) { return true; }
  return false;
}

function commandIndex(): number {
  for (let i = 0; i < process.argv.length; i++) {
    if (isSeedThat(process.argv[i])) { return i; }
  }
  return -1;
}

export function getSourceProject(options: Options): Project {
  const parameter = process.argv[commandIndex() + 2];
  const sourceString = parameter && !parameter.startsWith("-")
    ? parameter
    : options.defaults.seed;
  return decodeProject(sourceString);
}

export function getTargetProject(options: Options): Project {
  const parameter = process.argv[commandIndex() + 1];
  if (parameter == null || parameter.startsWith("-")) { throw new Error("First parameter should be the target library"); }
  const decoded = decodeProject(parameter);
  return {
    name: decoded.name,
    npmUser: decoded.npmUser || options.defaults.npmUser,
    gitUser: decoded.gitUser || options.defaults.gitUser
  };
}

export function decodeProject(projectParam: string = ""): Project {
  const split = projectParam.replace("@", "").split("/").filter(x => x.length > 0);
  const filled = fill(split);
  return {
    name: filled[2],
    npmUser: filled[1],
    gitUser: filled[0] || filled[1]
  };
}

function fill(strings: string[]): string[] {
  let result = strings;
  while (result.length < 3) {
    result = [undefined, ...result];
  }
  return result;
}
