import { Project } from "../project/project.type";

export function projectString(project: Project): string {
  return `${project.npmUser}/${project.name}`;
}
