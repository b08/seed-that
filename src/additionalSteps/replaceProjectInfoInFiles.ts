import { Project } from "../project/project.type";
import * as fse from "fs-extra";
import * as globby from "globby";
import { projectString } from "./projectString";
import { Options } from "../options/options.type";

const excludeFolders = [".git", "node_modules"];

export async function replaceProjectInfoInFiles(source: Project, target: Project, options: Options): Promise<void> {
  const sourceString = projectString(source);
  const targetString = projectString(target);
  const globs = [
    `./${target.name}/**/*(.)*`,
    ...excludeFolders.map(f => `!./${target.name}/${f}/**/*`)
  ];
  const files = await globby(globs);
  const promises = files.map(file => replaceInFile(file, sourceString, targetString, options));
  await Promise.all(promises);
}

async function replaceInFile(file: string, sourceString: string, targetString: string, options: Options): Promise<void> {
  const content = await fse.readFile(file, "utf8");
  const regex = new RegExp(sourceString, "g");
  let result = content.replace(regex, targetString);
  if (options.type != null && options.type.length > 0) {
    result = result.replace(/category\s*:\s*Category\.[a-z]*/, `category: Category.${options.type}`);
  }

  if (result === content) { return; }
  await fse.writeFile(file, result);
}
