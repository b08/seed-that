import { projectString } from "./projectString";
import { Project } from "../project/project.type";
import * as fse from "fs-extra";
import { Options } from "../options/options.type";

export async function replaceReadme(source: Project, target: Project, options: Options): Promise<void> {
  let content = `# @${projectString(target)}, seeded from @${projectString(source)}`;
  if (options.type != null && options.type.length > 0) {
    content += `, library type: ${options.type}`;
  }
  await fse.writeFile(`./${target.name}/README.md`, content, "utf8");
}
