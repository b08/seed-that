import * as fse from "fs-extra";
import { Options } from "../options/options.type";
import { decodeProject, getSourceProject } from "../project/getProjectFromArgs";
import { Project } from "../project/project.type";
import { projectString } from "./projectString";

export async function getSourceFromReadme(target: Project, options: Options): Promise<Project> {
  const readme = await fse.readFile(`./${target.name}/README.md`, "utf8");
  const proj = projectString(target).replace("/", "\/");
  const regex = new RegExp(`^# @${proj} seeded from @([^\\)]+)`);
  const match = readme.match(regex);
  if (!match) { return getSourceProject(options); }
  const matchText = match[1];
  return decodeProject(matchText);
}
