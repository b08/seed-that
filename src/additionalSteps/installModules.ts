import { spawnIt } from "@b08/spawn";
import { targetPath } from "../git/targetPath";
import { Project } from "../project/project.type";

export async function installNodeModules(target: Project): Promise<void> {
  const path = targetPath(target);
  console.log("Installing node modules");
  await spawnIt("npm", ["i"], { cwd: path });
  console.log("Finished installing node modules");
}
