export const constants = {
  allTs: "**/*.ts",
  allJs: "**/*.js",
  allFiles: "**/*.*",
  metricsFile: "metrics.json"
};
