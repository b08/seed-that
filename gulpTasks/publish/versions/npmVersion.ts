import * as fse from "fs-extra";
import { parseVersion } from "./parseVersion";
import { Version } from "./version";

const pkgFile = "package.json";

const regex = /"version": "([^"]+)"/;
export async function getNpmVersion(): Promise<Version> {
  const pkg = await readPkgFile();
  const m = pkg.match(regex);
  return parseVersion(m[1]);
}

const gitRepoRegex = /"repository":\s*{[^}]+"url":\s*"([^"]*)"/;
export async function getGitRepo(): Promise<string> {
  const pkg = await readPkgFile();
  const m = pkg.match(gitRepoRegex);
  return m[1];
}

async function readPkgFile(): Promise<string> {
  return await fse.readFile("package.json", "utf8");
}

export async function applyNpmVersion(version: Version): Promise<void> {
  const pkg = await readPkgFile();
  const replaced = pkg.replace(regex, `"version": "${version.major}.${version.minor}.${version.patch}"`);
  await fse.writeFile(pkgFile, replaced);
}
