import { Version } from "./version";

const regex = /(\d+).(\d+).(\d+)/;
export function parseVersion(version: string = ""): Version {
  const m = version.match(regex);
  const result = m
    ? { major: +m[1], minor: +m[2], patch: +m[3] }
    : zeroVersion();
  return result;
}

export function zeroVersion(): Version {
  return { major: 0, minor: 0, patch: 0 };
}
