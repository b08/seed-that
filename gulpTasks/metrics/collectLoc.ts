import { spawnIt } from "@b08/spawn";
import { settings } from "../settings";
import { setLoc } from "./saveMetrics";

const regex = /\s*Source\s*:\s*(\d+)\s*/;
export async function collectLoc(): Promise<void> {
  const locOutput = await spawnIt("npx", ["sloc", settings.src]);
  const grp = locOutput.match(regex);
  if (grp === null) { return; }
  const loc = +grp[1];
  setLoc(loc);
}
