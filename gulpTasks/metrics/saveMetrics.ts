import * as fse from "fs-extra";
import { constants } from "../settings";

const metrics = { loc: 0, tests: 0, coverage: 0 };

export function setLoc(loc: number): void {
  metrics.loc = loc;
}

export function setTests(tests: number): void {
  metrics.tests = tests;
}

export function setCoverage(coverage: number): void {
  metrics.coverage = coverage;
}

export async function saveMetrics(): Promise<void> {
  const json = JSON.stringify(metrics);
  await fse.writeFile(constants.metricsFile, json, "utf8");
}
