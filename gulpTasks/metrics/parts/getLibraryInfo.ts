import { getGitRepo } from "../../publish/versions/npmVersion";
import { Library } from "../types/libraryStatistic.type";
import { Category } from "../types/libraryCategory.enum";

export async function getLibraryInfo(): Promise<Library> {
  return {
    name: "@b08/seed-that",
    gitRepo: await getGitRepo(),
    npmUrl: "https://www.npmjs.com/package/@b08/seed-that",
    category: Category.support
  };
}
