import { parseVersion } from "../../publish/versions/parseVersion";
import { constants } from "../../settings";
import { getLibraryInfo } from "./getLibraryInfo";
import { LibraryStatistic } from "../types";
import * as fse from "fs-extra";

export async function getLibraryStatistic(): Promise<LibraryStatistic> {
  const json = await fse.readFile(constants.metricsFile, "utf8");
  return {
    library: await getLibraryInfo(),
    version: { ...parseVersion(process.env.version), publishedAt: today() },
    metrics: JSON.parse(json)
  };
}

function today(): { nDate: number } {
  const date = new Date();
  return { nDate: date.getFullYear() * 10000 + (date.getMonth() + 1) * 100 + date.getDate() };
}
