import * as request from "request-promise-native";
import { LibraryStatistic } from "../types";

export async function publishVersionStatistic(statistic: LibraryStatistic, serverUrl: string, user: string, password: string): Promise<void> {
  console.log("Trying to publish version statistic to " + serverUrl);
  const requestUrl = serverUrl + "/api/libraryStatistic";
  const options = {
    method: "POST",
    url: requestUrl,
    body: statistic,
    json: true,
    auth: { user, password }
  };
  try {
    await request(options);
    console.log("Successful");
  } catch (err) {
    console.log(err.message);
  }
}

