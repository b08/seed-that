import { getLibraryStatistic } from "./parts/getLibraryStatistic";
import { publishVersionStatistic } from "./parts/publishVersionStatistic";

export async function sendMetrics(): Promise<void> {
  const statistic = await getLibraryStatistic();
  const urls = process.env.statisticUrls.split(";").filter(i => i.length > 0);
  const user = getUserFromLibraryName(statistic.library.name);
  const password = process.env.statisticPassword;
  for (let url of urls) {
    await publishVersionStatistic(statistic, url, user, password);
  }
}

const userRegex = /@([^\/]+)/;
function getUserFromLibraryName(libraryName: string): string {
  return libraryName.match(userRegex)[1];
}
