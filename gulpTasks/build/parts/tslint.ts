import { spawn } from "@b08/spawn";

export async function tslint(): Promise<void> {
  await spawn("npx", "tslint", "-p", "tsconfig.json");
}
