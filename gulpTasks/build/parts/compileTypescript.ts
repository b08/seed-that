import { spawn } from "@b08/spawn";

export async function compileTypescript(): Promise<void> {
  await spawn("npx", "tsc");
}
