import { series } from "gulp";
import { compileTypescript } from "./parts";
import { preparation } from "./preparation";
import { collectLoc, saveMetrics } from "../metrics";

export const build = series(
  collectLoc,
  preparation,
  compileTypescript,
  saveMetrics
);

